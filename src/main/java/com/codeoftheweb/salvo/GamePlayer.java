package com.codeoftheweb.salvo;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
public class GamePlayer {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    private long id;
    private LocalDateTime joinDate;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="game_ID")
    private Game game;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="player_ID")
    private Player player;


    @OneToMany(mappedBy="gamePlayer", fetch=FetchType.EAGER)
    Set<Ship> ships;

    @OneToMany(mappedBy="gamePlayer", fetch=FetchType.EAGER)
    Set<Salvo>salvoes;

    public GamePlayer(Game game, Player player, LocalDateTime parse) {
        this.game = game;
        this.player = player;
        this.joinDate = parse;
    }

    public Set<Salvo> getSalvoes() {
        return salvoes;
    }



    public GamePlayer() {
    }



    public long getId() {
        return id;
    }

    public Game getGame() {
        return game;
    }



    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public LocalDateTime getJoinDate() {
        return joinDate;
    }



    public Map<String, Object> makeGamePlayerDTO() {
        Map<String, Object> dto = new LinkedHashMap<>();
        dto.put("id", this.getId());
        dto.put("player", this.getPlayer().makePlayerDTO());
        return dto;
    }

    public Set<Ship> getShips() {
        return ships;
    }



    public List<Map<String, Object>> makeGamePlayerSalvoesDTO(){

        return this.getSalvoes().stream().map(sv->sv.makeSalvoDTO()).collect(Collectors.toList());
    }

    /*public Map<String,Object> getScore(){

        Score score = this.getPlayer().getScore(this.getGame());
        return score.makeScoreDTO();
    }*/


}
