package com.codeoftheweb.salvo;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Entity
public class Salvo {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="gamePlayer_ID")
    private GamePlayer gamePlayer;

    private Long turn;

    @ElementCollection
    @JoinColumn(name = "salvoLocation")
    private List<String> location;

    public Salvo() {
    }

    public Salvo(GamePlayer gamePlayer, Long turn, List<String> location) {
        this.gamePlayer = gamePlayer;
        this.turn = turn;
        this.location = location;
    }

    public Long getId() {
        return id;
    }

    public GamePlayer getGamePlayer() {
        return gamePlayer;
    }


    public Long getTurn() {
        return turn;
    }

    public List<String> getLocation() {
        return location;
    }

    public Map<String, Object> makeSalvoDTO() {
    Map<String, Object> dto = new LinkedHashMap<String, Object>();

        dto.put("turn", this.getTurn());
        dto.put("player",getGamePlayer().getPlayer().getId());
        dto.put("locations",getLocation());
    return dto;
    }
}

