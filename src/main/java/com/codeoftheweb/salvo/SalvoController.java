package com.codeoftheweb.salvo;


import com.codeoftheweb.salvo.GamePlayer;
import com.codeoftheweb.salvo.GamePlayerRepository;
import com.codeoftheweb.salvo.GameRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.AuthenticationEventPublisher;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
    class SalvoController {

    @Autowired
      public GameRepository gameRepository;

    @Autowired
    public PlayerRepository playerRepository;

    @Autowired
    public GamePlayerRepository gamePlayerRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @RequestMapping("/games")
    public Map<String, Object> getAll(Authentication authentication){
        Map<String, Object> dto = new LinkedHashMap<String, Object>();
        if(!isGuest(authentication)){
            dto.put("player",playerRepository.findPlayerByUserName(authentication.getName()).makePlayerDTO());
        }
        else
            dto.put("player","Guest");
            dto.put("games",gameRepository.findAll().stream().map(game -> game.makeGameDTO()).collect(Collectors.toList()));
            return dto;
        }

    private boolean isGuest(Authentication authentication) {
        return authentication == null || authentication instanceof AnonymousAuthenticationToken;
    }

    @RequestMapping(value = "/games", method = RequestMethod.POST)
    public ResponseEntity<Map<String,Object>> createGame(Authentication authentication){

        if(isGuest(authentication)){
            return new ResponseEntity<>(makeMap("error","Unauthorized "), HttpStatus.UNAUTHORIZED);
        }

        Player player= playerRepository.findPlayerByUserName(authentication.getName());

        Game game = new Game(LocalDateTime.parse(LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME))) ;
        gameRepository.save(game);

        GamePlayer gamePlayer = new GamePlayer(game,player, LocalDateTime.parse(LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME)));
        gamePlayerRepository.save(gamePlayer);

        return new ResponseEntity<>(makeMap("gpid",gamePlayer.getId()),HttpStatus.OK);
    }

    @RequestMapping("/game_view/{gamePlayerId}")
    public ResponseEntity<Map<String,Object>> findGamePlayer( @PathVariable Long gamePlayerId, Authentication authentication){

            Map<String,Object> dto = new LinkedHashMap<String,Object>();
            Map<String,Object> hits = new LinkedHashMap<String,Object>();
            GamePlayer gamePlayer = gamePlayerRepository.findById(gamePlayerId).get();
            Player player = playerRepository.findPlayerByUserName(authentication.getName());

            if (gamePlayer.getPlayer().equals(player))  {
                dto.put("id", gamePlayer.getGame().getId());
                dto.put("created", gamePlayer.getGame().getCreationDate());
                dto.put("gameState", "PLACESHIPS");
                dto.put("gamePlayers",gamePlayer.getGame().getGamePlayers().stream().map(gp -> gp.makeGamePlayerDTO()).collect(Collectors.toList()));
                dto.put("ships",gamePlayer.getShips().stream().map(sh -> sh.makeShipDTO()).collect(Collectors.toList()));
                dto.put("salvoes",gamePlayer.getGame().getGamePlayers().stream().map(gp-> gp.makeGamePlayerSalvoesDTO()).flatMap(x -> x.stream()).collect(Collectors.toList()));
                hits.put("self",new ArrayList<String>());
                hits.put("opponent",new ArrayList<String>());
                dto.put("hits",hits);
                return new ResponseEntity<Map<String, Object>>(dto, HttpStatus.OK);
          }
            else
                return new ResponseEntity<>(makeMap("error", "error"), HttpStatus.UNAUTHORIZED);
        }

    private Map<String, Object> makeMap(String key, Object value) {
        Map<String, Object> map = new HashMap<>();
        map.put(key, value);
        return map;
    }

     @RequestMapping(path = "/players", method = RequestMethod.POST)
     public ResponseEntity<Map<String,Object>> createPlayer(@RequestParam String email, @RequestParam String password) {

        if (email.isEmpty() || password.isEmpty()) {
            return new ResponseEntity<>(makeMap("error","Missing data"), HttpStatus.FORBIDDEN);
        }

        if (playerRepository.findPlayerByUserName(email) !=  null) {
            return new ResponseEntity<>(makeMap("error","Name already in use"), HttpStatus.FORBIDDEN);
            }

            playerRepository.save(new Player(email, passwordEncoder.encode(password)));
            return new ResponseEntity<>(makeMap("OK","Todo bien"), HttpStatus.CREATED);

        }

     @RequestMapping(path = "/game/{gameId}/players", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> joinGame(@PathVariable Long gameId, Authentication authentication){

        Game game = gameRepository.findById(gameId).get();
        Player player = playerRepository.findPlayerByUserName(authentication.getName());
         if(isGuest(authentication)){
             return new ResponseEntity<>(makeMap("error","Unauthorized "), HttpStatus.UNAUTHORIZED);
         }

         else if (game == null)
             return new ResponseEntity<>(makeMap("error","No such game"), HttpStatus.FORBIDDEN);

         else if (game.getGamePlayers().size()==2)
             return new ResponseEntity<>(makeMap("error","Game is full"), HttpStatus.FORBIDDEN);
        else {
            GamePlayer gamePlayer = new GamePlayer(game,player,LocalDateTime.parse(LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME)));
            gamePlayerRepository.save(gamePlayer);
            return new ResponseEntity<>(makeMap("gpid", gamePlayer.getId()),HttpStatus.OK);
         }

     }
}


